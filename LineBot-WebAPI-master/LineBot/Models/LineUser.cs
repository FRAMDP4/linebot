﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LineBot.Models
{
    public class LineUser
    {
        public string LineID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string PhoneNumber { get; set; }
        public string BinusianID { get; set; }
        public int RoleID { get; set; }
        public int SpecificRoleID{ get; set; }
        public int OTP { get; set; }
        public string EmployeeID { get; set; }
        public string RegistrationDate { get; set; }
    }
}