﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LineBot.Models
{
    public class MulticastMessageRequest
    {
        public string type
        {
            get;set;
        }
        public string text
        {
            get;set;
        }
    }
}