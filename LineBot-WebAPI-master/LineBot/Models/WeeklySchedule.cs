﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LineBot.Models
{
    public class WeeklySchedule
    {
        public string Date { get; set; }
        public int Day { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string Course { get; set; }
        public string Class { get; set; }
        public string Room { get; set; }
        public string Campus { get; set; }
        public string Component { get; set; }
    }
}