﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LineBot.Models
{
    public class BroadcastRequest
    {
        public string apiKey
        {
            get;
            set;
        }
        public BroadcastRequestData data
        {
            get;
            set;
        }
    }
}