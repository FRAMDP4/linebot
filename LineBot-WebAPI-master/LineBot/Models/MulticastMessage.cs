﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LineBot.Models
{
    public class MulticastMessage
    {
        public MulticastMessage()
        {
            to = new List<string>();
            messages = new List<MulticastMessageRequest>();
        }
        public List<String> to {
            get;
            set;
        }

        public List<MulticastMessageRequest> messages
        {
            get;
            set;
        }
    }
}