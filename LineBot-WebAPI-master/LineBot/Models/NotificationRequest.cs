﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LineBot.Models
{
    public class NotificationRequest
    {
        public string apiKey { get; set; }
        public string command { get; set; }
    }
}