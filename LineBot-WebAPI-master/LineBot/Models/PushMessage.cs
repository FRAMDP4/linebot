﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LineBot.Models
{
    public class PushMessage
    {
        public PushMessage()
        {
            messages = new List<MessageObject>();
        }
        public string to { get; set; }
        public List<MessageObject> messages { get; set; }
    }

    public class MessageObject
    {
        public string type { get; set; }
        public string text { get; set; }
    }
}