﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using LineBot.Models;
using LineBot.Utility;
using LineBot.Controllers;
using LineBot.DAL;

namespace LineBot.BLL
{
    public class LineHandler : ILineHandler
    {
        public async Task ProcessMessage(WebhookModel value)
        {
            foreach (var msg in value.events)
            {
                switch (msg.type)
                {
                    case "follow":
                        var res = await APIHelper.ReplyMessage(HandleFollowAndJoin(msg));
                        break;
                    case "postback":
                        //default handler
                        await APIHelper.ReplyMessage(GetTextReply(msg.replyToken, msg.postback.data));
                        break;
                    case "join":
                        await APIHelper.ReplyMessage(HandleFollowAndJoin(msg));
                        break;
                    case "message":
                        var r = HandleMessageObject(msg);
                        if (r != null)
                            await APIHelper.ReplyMessage(r);
                        break;
                    default:
                        await APIHelper.ReplyMessage(GetTextReply(msg.replyToken, "not support"));
                        break;
                }
            }
        }

        private ReplyModel HandleFollowAndJoin(Event msg)
        {
            return ProcessQuery(msg.replyToken, "start", msg.source.userId);
        }

        private ReplyModel HandleMessageObject(Event msg)
        {
            //handle disini
            switch (msg.message.type)
            {
                case "text":
                    return ProcessQuery(msg.replyToken, msg.message.text, msg.source.userId);
                case "image":
                case "sticker":
                case "video":
                case "audio":
                case "location":
                default:
                    //ignoring other
                    return null;
            }
        }

        private ReplyModel ProcessQuery(string token, string msg, string id)
        {
            string m = msg.ToLower();

            //call insertLog() function
            LineData.insertLog(id, msg);
            switch (m)
            {
                case "reset":
                    return new UtilController(token, id, msg).Reset();
                case "ping":
                    return new UtilController(token, id, msg).Ping();
                case "help":
                          return new UtilController(token, id, msg).Help();
                case "course":
                    return new DashboardController(token, id, msg).Course();
                case "today":
                    return new DashboardController(token, id, msg).Today();
                case "weekly":
                    return new DashboardController(token, id, msg).Weekly();
                case "news":
                    return new NewsController(token, id, msg).News();
                case "message":
                    return new MessageController(token, id, msg).Message();
                case "forum":
                    return new ForumController(token, id, msg).Forum();
                case "finance":
                    return new FinanceController(token, id, msg).Finance();
                case "gslc":
                    return new GslcController(token, id, msg).GSLC();
                case "exam":
                    return new ExamController(token, id, msg).ExamSchedule();
                case "score":
                    return new ScoreController(token, id, msg).ScoreExam();
                case "settings":
                    return new SettingsController(token, id, msg).Settings();
                case "examnotification":
                    return new SettingsController(token, id, msg).NotificationSettingSaved();
                case "paymentnotification":
                    return new SettingsController(token, id, msg).NotificationSettingSaved();
                case "msgnotification":
                    return new SettingsController(token, id, msg).NotificationSettingSaved();
                case "classschednotification":
                    return new SettingsController(token, id, msg).NotificationSettingSaved();
                case "start":
                default:
                    BaseController controller = new BaseController(token, id, msg);
                    return controller.HandleMessage();
            } 
        }

        private ReplyModel GetTextReply(string token, string text)
        {
            return new ReplyModel()
            {
                replyToken = token,
                messages = new List<ReplyMessage>()
                {
                    new ReplyMessage()
                    {
                        type = "text",
                        text = text
                    }
                }
            };
        }
    }
}