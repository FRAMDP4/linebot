﻿using LineBot.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace LineBot.Utility
{
    public class MessageBroadcaster
    {

        public static async Task<HttpResponseMessage> SendBroadcast(List<String> LineIds, String message)
        {
            List<MulticastMessage> messages = new List<MulticastMessage>();

            MulticastMessage m = new MulticastMessage();
            MulticastMessageRequest msg = new MulticastMessageRequest();
            msg.type = "text";
            msg.text = message;
            m.messages.Add(msg);
            foreach (string lineID in LineIds)
            {
                if(m.to.Count < Util.LINE_MULTICAST_RECIPIENT_LIMIT)
                {
                    m.to.Add(lineID);
                }
                else
                {
                    messages.Add(m);
                    m = new MulticastMessage();
                    m.messages.Add(msg);
                    m.to.Add(lineID);
                }
            }
            messages.Add(m);
            foreach (MulticastMessage multicastMessage in messages)
            {
                using (HttpClient client = new HttpClient())
                {
                    var json = JsonConvert.SerializeObject(multicastMessage);
                    JsonConvert.SerializeObject(multicastMessage, new JsonSerializerSettings()
                    {
                        NullValueHandling = NullValueHandling.Ignore
                    });
                    client.DefaultRequestHeaders.Add("Authorization", $"Bearer {Util.LINE_CHANNEL_TOKEN}");
                    //
                    var r = await client.PostAsync(Util.LINE_MULTICAST_URL,
                        new StringContent(json, Encoding.UTF8, "application/json"));
                    Console.Write(r.Content);
                }
            }
            return new HttpResponseMessage(System.Net.HttpStatusCode.OK);
        }
    }
}