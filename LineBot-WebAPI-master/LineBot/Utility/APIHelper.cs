﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using LineBot.Models;
using Newtonsoft.Json;

namespace LineBot.Utility
{
    public class APIHelper
    {
        public static async Task<HttpResponseMessage> ReplyMessage(ReplyModel reply)
        {
            using (HttpClient client = new HttpClient()) 
            {
                var json = JsonConvert.SerializeObject(reply);
                client.DefaultRequestHeaders.Add("Authorization", $"Bearer {Util.LINE_CHANNEL_TOKEN}");
                var msg = reply.messages.First().text;
                //
                //await client.PostAsync(Util.LINE_REPLY_URL,
                    //new StringContent(json, Encoding.UTF8, "application/json"));
                return new HttpResponseMessage(System.Net.HttpStatusCode.OK);
            }
        }
    }
}