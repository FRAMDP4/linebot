﻿using LineBot.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace LineBot.Utility
{
    public class NotificationSender
    {
        public static async Task<HttpResponseMessage> sendNotification(List<PushMessage> messages)
        {
            foreach (PushMessage msg in messages)
            {
                using (HttpClient client = new HttpClient())
                {
                    var json = JsonConvert.SerializeObject(msg);
                    JsonConvert.SerializeObject(msg, new JsonSerializerSettings()
                    {
                        NullValueHandling = NullValueHandling.Ignore
                    });
                    client.DefaultRequestHeaders.Add("Authorization", $"Bearer {Util.LINE_CHANNEL_TOKEN}");
                    //
                    var r = await client.PostAsync(Util.LINE_PUSH_URL,
                        new StringContent(json, Encoding.UTF8, "application/json"));
                    Console.Write(r.Content);
                }
            }
            return new HttpResponseMessage(System.Net.HttpStatusCode.OK);
        }

        public static async Task<HttpResponseMessage> sendNotification(PushMessage msg)
        {
            //foreach (PushMessage msg in messages)
            //{
                using (HttpClient client = new HttpClient())
                {
                    var json = JsonConvert.SerializeObject(msg);
                    JsonConvert.SerializeObject(msg, new JsonSerializerSettings()
                    {
                        NullValueHandling = NullValueHandling.Ignore
                    });
                    client.DefaultRequestHeaders.Add("Authorization", $"Bearer {Util.LINE_CHANNEL_TOKEN}");
                    //
                    var r = await client.PostAsync(Util.LINE_PUSH_URL,
                        new StringContent(json, Encoding.UTF8, "application/json"));
                    Console.Write(r.Content);
                }
            //}
            return new HttpResponseMessage(System.Net.HttpStatusCode.OK);
        }
    }
}