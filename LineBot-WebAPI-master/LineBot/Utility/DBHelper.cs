﻿using LineBot.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace LineBot.Utility
{
    public class DBHelper
    {
        public static List<string> GetAllLineUserID()
        {
            List<string> ret = new List<string>();
            using (var conn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["LINE_CONNECTION_STRING"]))
            {
                var command = new SqlCommand("bn_Line_Agent_GetAllLineID", conn);
                command.CommandType = CommandType.StoredProcedure;
                conn.Open();

                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    ret.Add(reader["LineID"].ToString());
                }
                conn.Close();
                return ret;
            }  
        }
    }
}