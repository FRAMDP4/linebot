﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LineBot.Utility
{
    public class Util
    {
        public static readonly string LINE_MULTICAST_URL = "https://api.line.me/v2/bot/message/multicast";
        public static readonly string LINE_REPLY_URL = "https://api.line.me/v2/bot/message/reply";
        public static readonly string LINE_PUSH_URL = "https://api.line.me/v2/bot/message/push";

        public static readonly string LINE_CHANNEL_TOKEN = System.Configuration.ConfigurationManager.AppSettings["LINE_CHANNEL_TOKEN"];
        public static readonly string LINE_CHANNEL_SECRET = System.Configuration.ConfigurationManager.AppSettings["CHANNEL_SECRET"];
        public static readonly int LINE_MULTICAST_RECIPIENT_LIMIT = int.Parse(System.Configuration.ConfigurationManager.AppSettings["LINE_MULTICAST_RECIPIENT_LIMIT"]);
        public static readonly string API_KEY = System.Configuration.ConfigurationManager.AppSettings["API_KEY"];
    }
}