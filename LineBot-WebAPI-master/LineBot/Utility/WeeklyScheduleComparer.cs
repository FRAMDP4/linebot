﻿using LineBot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LineBot.Utility
{
    public class WeeklyScheduleComparer : Comparer<WeeklySchedule>
    {
        public override int Compare(WeeklySchedule x, WeeklySchedule y)
        {
            if (x.Day > y.Day)
                return 1;
            else if (x.Day == y.Day)
                return 0;
            return -1;
        }
    }
}