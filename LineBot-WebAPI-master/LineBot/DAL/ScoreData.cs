﻿using System.Data;
using System.Data.SqlClient;

namespace LineBot.DAL
{
    public class ScoreData
    {
        public static string GetScoreExam(string employeeID)
        {
            var acadCareer = LMSData.GetAcadCareer(employeeID);
            var strm = LMSData.GetPeriod(acadCareer, employeeID);
            using (var conn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["BM_SCORE_CONNECTION_STRING"]))
            {
                var command = new SqlCommand("Student_ListScore_Per_Strm", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("binusianid", employeeID));
                command.Parameters.Add(new SqlParameter("STRM", "1610"));
                //Buat Test
                //command.Parameters.Add(new SqlParameter("binusianid", "1301009975"));
                //command.Parameters.Add(new SqlParameter("STRM", "1510"));
                conn.Open();

                var reader = command.ExecuteReader();
                string ret = "";
                string desc = "";
                while (reader.Read())
                {
                    if (ret != "")
                        ret += "\n";

                    if (desc != reader["DESCR"].ToString())
                    {
                        desc = reader["DESCR"].ToString();
                        ret += desc + "\n Final Score : " + reader["GRADE_AVG_CURRENT"].ToString() + "\n Final Grade : " + reader["COURSE_GRADE_CALC"];
                    }

                }

                if (ret == "")
                    ret = "No Score Has Been Uploaded";
                conn.Close();
                return ret;
            }
        }
    }
}