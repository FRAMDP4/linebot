﻿using LineBot.Models;
using LineBot.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace LineBot.DAL
{
    public class LMSData
    {
        public static string GetCourse(string employeeID)
        {
            string acadCareer = GetAcadCareer(employeeID);
            using (var conn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["BM_CONNECTION_STRING"]))
            {
                var command = new SqlCommand("bn_Student_GetCourses", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("NIM", employeeID));
                command.Parameters.Add(new SqlParameter("ACAD", acadCareer));

                conn.Open();
                var reader = command.ExecuteReader();
                string ret = "";
                while (reader.Read())
                {
                    if (ret != "")
                        ret += "\n";
                    ret += reader["COURSEID"].ToString() + " - " + reader["COURSENAME"] + " - "
                        + reader["CLASS_SECTION"];
                }
                conn.Close();
                return ret;
            }
        }

        public static string GetAcadCareer(string employeeID)
        {
            string acadCareer = "RS1";
            using (var conn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["BM_CONNECTION_STRING"]))
            {
                var command = new SqlCommand("bn_Student_GetActiveAcadCareer", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("UserID", employeeID));
                conn.Open();

                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                    acadCareer = reader["ACAD_CAREER"].ToString();
                else
                {
                    conn.Close();
                    return acadCareer;
                }
                conn.Close();
            }
            return acadCareer;
        }

        public static string GetToday(string employeeID)
        {
            string acadCareer = GetAcadCareer(employeeID);
            using (var conn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["BM_CONNECTION_STRING"]))
            {
                var command = new SqlCommand("bn_Student_GetClassScheduleNextAgenda", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("StudentID", employeeID));
                command.Parameters.Add(new SqlParameter("ACAD_CAREER", acadCareer));
                conn.Open();

                var reader = command.ExecuteReader();
                string ret = "";
                while (reader.Read())
                {
                    if (ret != "")
                        ret += "\n";
                    else
                    {
                        ret = "Your list for today:\n";
                    }
                    var room = reader["Room"] == DBNull.Value ? "GSLC" : reader["Room"].ToString();
                    ret += reader["Shift"].ToString() + " | " + room + " | " + reader["CourseName"].ToString() + " - " + reader["Campus"];
                }
                if (ret == "")
                    ret = "Your day seems bright! :)";
                conn.Close();
                return ret;
            }
        }

        public static List<PushMessage> GetClassScheduleNextAgenda()
        {
            string acadCareer = "RS1";
            using (var conn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["LINE_CONNECTION_STRING"]))
            {
                var command = new SqlCommand("bn_Line_Agent_GetClassScheduleNextAgenda", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("ACAD_CAREER", acadCareer));
                conn.Open();

                SqlDataReader reader = command.ExecuteReader();
                List<PushMessage> output = new List<PushMessage>();
                PushMessage data = new PushMessage();
                MessageObject msg = new MessageObject();
                msg.type = "text";
                string ret = "";
                string binusianID = "";

                while (reader.Read())
                {
                    if (binusianID != reader["BinusianID"].ToString())
                    {
                        if (binusianID != "")
                        {
                            data.messages.Add(msg);
                            output.Add(data);
                        }

                        data = new PushMessage();
                        ret = "";
                        binusianID = reader["BinusianID"].ToString();
                        data.to = reader["LineID"].ToString();
                    }

                    if (reader["CourseName"] == DBNull.Value)
                        continue;

                    if (ret != "")
                        ret = "\n";
                    else
                        ret = "Your list for today:\n";

                    var room = reader["Room"] == DBNull.Value ? "GSLC" : reader["Room"].ToString();

                    ret += reader["Shift"].ToString() + " | " + room + " | " + reader["CourseName"].ToString() + " - " + reader["Campus"];

                    msg.text = ret;
                }
                data.messages.Add(msg);
                output.Add(data);

                conn.Close();
                return output;
            }
        }

        public static string GetPeriod(string acadCareer, string employeeID)
        {
            string period = "1620";
            using (var conn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["BM_CONNECTION_STRING"]))
            {
                var command = new SqlCommand("bn_Schedule_GetPeriods", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("AcadCareer", acadCareer));
                command.Parameters.Add(new SqlParameter("BinusID", employeeID));
                conn.Open();

                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                    period = reader["STRM"].ToString();
                else
                {
                    conn.Close();
                    return period;
                }
                conn.Close();
            }
            return period;
        }

        public static string GetGeneralActivePeriod()
        {
            string acadCareer = "RS1";
            string period = "1620";

            using (var conn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["BM_CONNECTION_STRING"]))
            {
                var command = new SqlCommand("bn_General_ActivePeriod", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("Acad_career", acadCareer));
                conn.Open();

                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                    period = reader["STRM"].ToString();
                else
                {
                    conn.Close();
                    return period;
                }
                conn.Close();
                return period;
            }
        }

        public static string GetWeekly(string employeeID, string binusianID)
        {
            string acadCareer = GetAcadCareer(employeeID);
            string period = GetPeriod(acadCareer, employeeID);
            using (var conn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["BM_CONNECTION_STRING"]))
            {
                var command = new SqlCommand("bn_Student_GetMasterSchedule", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("UserID", binusianID));
                command.Parameters.Add(new SqlParameter("AcadCareer", acadCareer));
                command.Parameters.Add(new SqlParameter("Strm", period));
                conn.Open();

                var reader = command.ExecuteReader();
                string ret = "";
                List<WeeklySchedule> scheduleList = new List<WeeklySchedule>();
                while (reader.Read())
                {
                    WeeklySchedule s = new WeeklySchedule();
                    s.Date = reader["Date"].ToString();
                    s.Day = int.Parse(reader["Day"].ToString());
                    s.StartTime = reader["StartTime"].ToString();
                    s.EndTime = reader["EndTime"].ToString();
                    s.Course = reader["Course"].ToString();
                    s.Class = reader["Class"].ToString();
                    s.Room = reader["Room"].ToString();
                    s.Campus = reader["Campus"].ToString();
                    s.Component = reader["Component"].ToString();

                    scheduleList.Add(s);
                }
                scheduleList.Sort(new WeeklyScheduleComparer());
                for (int i = 0; i < scheduleList.Count(); i++)
                {
                    var startTime = Convert.ToDateTime(scheduleList[i].StartTime);
                    var endTime = Convert.ToDateTime(scheduleList[i].EndTime);
                    //cek < 10
                    var shift = ((startTime.Hour < 10 ? "0" + startTime.Hour : "" + startTime.Hour) + ":"
                        + (startTime.Minute < 10 ? "0" + startTime.Minute : "" + startTime.Minute) + "-"
                        + (endTime.Hour < 10 ? "0" + endTime.Hour : "" + endTime.Hour) + ":"
                        + (endTime.Minute < 10 ? "0" + endTime.Minute : "" + endTime.Minute));
                    ret += scheduleList[i].Date + "|" + shift + "|" + scheduleList[i].Room + "|\n" + scheduleList[i].Course + "-" + scheduleList[i].Campus + "\n";
                }

                conn.Close();
                return ret;
            }

        }

        public static string GetNews(string binusianID, int roleID)
        {
            using (var conn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["BM_CONNECTION_STRING"]))
            {
                var command = new SqlCommand("Student_GetNewsAndEvents", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("StudentID", binusianID));
                command.Parameters.Add(new SqlParameter("RoleID", roleID));
                command.Parameters.Add(new SqlParameter("Page", 1));
                command.Parameters.Add(new SqlParameter("NewsType", 1));
                conn.Open();

                var reader = command.ExecuteReader();
                string ret = "";
                while (reader.Read())
                {
                    if (ret != "")
                        ret += "\n";
                    ret += reader["Title"].ToString() + ": " + reader["Description"];
                }
                if (ret == "")
                    ret = "No news Available";
                conn.Close();
                return ret;
            }
        }

        public static string GetMessage(string employeeID, int specificRoleID)
        {
            using (var conn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["BM_MESSAGE_CONNECTION_STRING"]))
            {
                var command = new SqlCommand("bn_GetInbox", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("BinusID", employeeID));
                command.Parameters.Add(new SqlParameter("RoleID", specificRoleID));
                conn.Open();

                var reader = command.ExecuteReader();
                string ret = "";
                int count = 0;
                while (reader.Read())
                {
                    if (ret != "")
                        ret += "\n";
                    if (reader["IsRead"].ToString() == "0")
                    {
                        if (reader["Sender"].ToString().IndexOf('-') >= 0) {
                            ret += (reader["Sender"].ToString().Substring(reader["Sender"].ToString().IndexOf('-'))) + " - "
                                + reader["Subject"] + " - " + reader["ReceivedDate"] + " [NEW]";
                        }
                        else
                        {
                            ret += "- " + (reader["Sender"].ToString()) + " - "
                                + reader["Subject"] + " - " + reader["ReceivedDate"] + " [NEW]";
                        }
                    }
                    else
                    {
                        if (reader["Sender"].ToString().IndexOf('-') >= 0)
                        {
                            ret += (reader["Sender"].ToString().Substring(reader["Sender"].ToString().IndexOf('-'))) + " - "
                                + reader["Subject"] + " - " + reader["ReceivedDate"];
                        }
                        else
                        {
                            ret += "- " + (reader["Sender"].ToString()) + " - "
                                + reader["Subject"] + " - " + reader["ReceivedDate"];
                        }
                    }
                    if (++count == 10)
                        break;
                }
                if (ret == "")
                    ret = "No Message Available";
                conn.Close();
                return ret;
            }
        }


        public static PushMessage GetNewestMessage(string employeeID, int specificRoleID)
        {
            using (var conn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["BM_MESSAGE_CONNECTION_STRING"]))
            {
                var command = new SqlCommand("bn_GetInbox", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("BinusID", employeeID));
                command.Parameters.Add(new SqlParameter("RoleID", specificRoleID));
                conn.Open();

                var reader = command.ExecuteReader();
                PushMessage data = new PushMessage();
                MessageObject ret = new MessageObject();
                ret.type = "text";
                if (reader.Read())
                {
                    data.to = LineData.GetLineID(employeeID); 
                    ret.text = (reader["Sender"].ToString().Substring(reader["Sender"].ToString().IndexOf('-'))) + " - "
                        + reader["Subject"] + " - " + reader["ReceivedDate"] + " [NEW]";
                }
                data.messages.Add(ret);
                conn.Close();
                return data;
            }
        }

        public static string GetGSLC(string employeeID, int specificRoleID)
        {
            using (var conn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["BM_CONNECTION_STRING"]))
            {
                var command = new SqlCommand("bn_calendar_classSchedule_GetStudentClassSchedule", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("BinusID", employeeID));
                command.Parameters.Add(new SqlParameter("SpecificRole", specificRoleID));
                conn.Open();

                var reader = command.ExecuteReader();
                string ret = "";
                int count = 0;
                while (reader.Read())
                {
                    if (ret != "")
                        ret += "\n";
                    //TODO validasi waktunya lebih dari yang sekarang
                    var startTime = Convert.ToDateTime(reader["START_DT"]);
                    if (DateTime.Compare(startTime, DateTime.Today) >= 0)
                    {
                        if (reader["N_DELIVERY_MODE"].ToString() != "GSLC")
                            continue;
                        ret += reader["COURSE_TITLE_LONG"] + " - " + reader["CLASS_SECTION"] + " - " + startTime +
                        " (" + reader["MEETING_TIME_START"].ToString() + " - " + reader["MEETING_TIME_END"] + ")";
                        count++;
                    }
                    if (count == 3)
                        break;

                }

                if (ret == "")
                    ret = "No Upcoming GSLC! :(";
                conn.Close();
                return ret;
            }
        }
    }
}