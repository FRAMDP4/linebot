﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace LineBot.DAL
{
    public class ForumData
    {
        private static List<string> GetForumCourses(string employeeID, string acadCareer, string strm)
        {
            List<string> courseIds = new List<string>();
            using (var conn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["BM_FORUM_CONNECTION_STRING"]))
            {
                var command = new SqlCommand("Forum_GetCourse", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("StudentID", employeeID));
                command.Parameters.Add(new SqlParameter("LecturerID", ""));
                command.Parameters.Add(new SqlParameter("AcadCareer", acadCareer));
                command.Parameters.Add(new SqlParameter("Strm", strm));

                conn.Open();
                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    courseIds.Add(reader["ID"].ToString());
                }

                conn.Close();
                return courseIds;
            }
        }

        private static List<string> GetForumClass(string employeeID, string acadCareer, string strm, string courseID)
        {
            List<string> classIDs = new List<string>();
            using (var conn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["BM_FORUM_CONNECTION_STRING"]))
            {
                var command2 = new SqlCommand("Forum_GetClass", conn);
                command2.CommandType = CommandType.StoredProcedure;
                command2.Parameters.Add(new SqlParameter("ExternalSystemID", employeeID));
                command2.Parameters.Add(new SqlParameter("KdDsn", ""));
                command2.Parameters.Add(new SqlParameter("AcadCareer", acadCareer));
                command2.Parameters.Add(new SqlParameter("strm", strm));
                command2.Parameters.Add(new SqlParameter("CrseID", courseID));

                conn.Open();
                var reader = command2.ExecuteReader();
                while (reader.Read())
                {
                    classIDs.Add(reader["ID"].ToString());
                }
                conn.Close();
                return classIDs;
            }
        }

        public static string GetForum(string employeeID)
        {
            string acadCareer = LMSData.GetAcadCareer(employeeID);
            string strm = LMSData.GetPeriod(acadCareer, employeeID);
            string ret = "";
            List<string> courseIds = GetForumCourses(employeeID, acadCareer, strm);

            foreach (string courseID in courseIds)
            {
                List<string> classIds = GetForumClass(employeeID, acadCareer, strm, courseID);
                foreach (string classID in classIds)
                {
                    using (var conn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["BM_FORUM_CONNECTION_STRING"]))
                    {
                        var command3 = new SqlCommand("Forum_GetThread", conn);
                        command3.CommandType = CommandType.StoredProcedure;
                        command3.Parameters.Add(new SqlParameter("BinusID", employeeID));
                        command3.Parameters.Add(new SqlParameter("ForumTypeID", "1"));
                        command3.Parameters.Add(new SqlParameter("AcadCareer", acadCareer));
                        command3.Parameters.Add(new SqlParameter("Strm", strm));
                        command3.Parameters.Add(new SqlParameter("CrseID", courseID));
                        command3.Parameters.Add(new SqlParameter("ClassSection", classID));

                        conn.Open();
                        var reader3 = command3.ExecuteReader();
                        while (reader3.Read())
                        {
                            if (ret != "")
                                ret += "\n";
                            ret += reader3["ForumThreadTitle"].ToString();
                        }
                        conn.Close();
                    }
                }
            }
            return ret.ToString().Replace("%20", " ");
        }
    }
}