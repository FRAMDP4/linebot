﻿using System.Data;
using System.Data.SqlClient;

namespace LineBot.DAL
{
    public class FinanceData
    {
        public static string GetFinance(string binusianID, string employeeID)
        {
            string ret = GetFinanceSummary(binusianID);

            using (var conn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["BM_CONNECTION_STRING"]))
            {
                var command = new SqlCommand("bn_Financial_student_GetFinancialSummary", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("BinusianID", employeeID));
                conn.Open();

                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    ret += reader["item_term"].ToString() + " - " + reader["descr"] + " (" + reader["descr_ind"].ToString()
                        + ") \nDueDate: " + reader["due_dt"].ToString().Substring(0, 10) + " | Charge: " + reader["item_amt"]
                        + " | Payment: " + reader["applied_amt"] + "\n\n";
                }
                conn.Close();
                return ret;
            }
        }

        private static string GetFinanceSummary(string binusianID)
        {
            using (var conn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["BM_CONNECTION_STRING"]))
            {
                var command = new SqlCommand("sprBLMSLoadTotalFinanceSummary", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("priod", ""));
                command.Parameters.Add(new SqlParameter("kdsem", ""));
                command.Parameters.Add(new SqlParameter("tglawal", ""));
                command.Parameters.Add(new SqlParameter("tglakhir", ""));
                command.Parameters.Add(new SqlParameter("nim", binusianID));
                conn.Open();

                var reader = command.ExecuteReader();
                string ret = "";
                if (reader.Read())
                {
                    ret = "Summary: Charge: " + reader["charge"].ToString() + "\nPayment: "
                        + reader["payment"].ToString() + "\nDeposit: " + reader["deposit"].ToString() + "\n\n";
                }
                else
                    ret = "";
                conn.Close();
                return ret;
            }
        }
    }
}