﻿using LineBot.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace LineBot.DAL
{
    public class LineData
    {
        public static LineUser CheckUser(string id)
        {
            using (var conn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["LINE_CONNECTION_STRING"]))
            {
                var command = new SqlCommand("bn_Line_CheckUser", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("LineID", id));
                conn.Open();

                LineUser user = null;
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    user = new LineUser();
                    user.LineID = reader["LineID"].ToString();
                    user.BinusianID = reader["BinusianID"].ToString();
                    user.EmployeeID = reader["EmployeeID"].ToString();
                    user.FirstName = reader["FirstName"].ToString();
                    user.LastName = reader["LastName"].ToString();
                    if (reader["OTP"] == DBNull.Value)
                        user.OTP = -1;
                    else
                        user.OTP = int.Parse(reader["OTP"].ToString());
                    user.PhoneNumber = reader["PhoneNumber"].ToString();
                    user.RegistrationDate = reader["RegistrationDate"].ToString();
                    user.Username = reader["Username"].ToString();
                    if (reader["RoleID"] != DBNull.Value)
                        user.RoleID = int.Parse(reader["RoleID"].ToString());
                    if (reader["SpecificRoleID"] != DBNull.Value)
                        user.SpecificRoleID = int.Parse(reader["SpecificRoleID"].ToString());
                }
                conn.Close();

                return user;
            }
        }

        public static void Register(string id, int otp)
        {
            if (otp < 0)
                otp *= -1;
            using (var conn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["LINE_CONNECTION_STRING"]))
            {
                var command = new SqlCommand("bn_Line_Register", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("LineID", id));
                command.Parameters.Add(new SqlParameter("OTP", otp));
                conn.Open();

                command.ExecuteNonQuery();
                conn.Close();
            }
        }

        public static void UpdatePhoneNumber(string id, string phoneNumber)
        {
            using (var conn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["LINE_CONNECTION_STRING"]))
            {
                var command = new SqlCommand("bn_Line_UpdatePhoneNumber", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("LineID", id));
                command.Parameters.Add(new SqlParameter("PhoneNumber", phoneNumber));
                conn.Open();

                command.ExecuteNonQuery();
                conn.Close();
            }
        }

        public static void UpdateNIM(string id, string NIM)
        {
            using (var conn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["LINE_CONNECTION_STRING"]))
            {
                var command = new SqlCommand("bn_Line_UpdateNIM", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("LineID", id));
                command.Parameters.Add(new SqlParameter("NIM", NIM));
                conn.Open();

                command.ExecuteNonQuery();
                conn.Close();
            }
        }

        public static void validateOTP(string id, int OTP)
        {
            using (var conn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["LINE_CONNECTION_STRING"]))
            {
                var command = new SqlCommand("bn_Line_ValidateOTP", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("LineID", id));
                command.Parameters.Add(new SqlParameter("OTP", OTP));
                conn.Open();

                command.ExecuteNonQuery();
                conn.Close();
            }
        }

        public static void resetUser(string id)
        {
            using (var conn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["LINE_CONNECTION_STRING"]))
            {
                var command = new SqlCommand("bn_Line_ResetUser", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("LineID", id));
                conn.Open();

                command.ExecuteNonQuery();
                conn.Close();
            }
        }

        //writes log
        public static void insertLog(string id, string query)
        {
            using (var conn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["LINE_CONNECTION_STRING"]))
            {
                var command = new SqlCommand("bn_Line_Insertlog", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("LineID", id));
                command.Parameters.Add(new SqlParameter("query", query));
                conn.Open();

                command.ExecuteNonQuery();
                conn.Close();
            }
        }

        public static string GetLineID(string employeeID)
        {
            string lineID = "";
            using (var conn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["LINE_CONNECTION_STRING"]))
            {
                var command = new SqlCommand("bn_Line_GetLineIDByBinusID", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("BinusID", employeeID));
                conn.Open();

                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                    lineID = reader["LineID"].ToString();
                conn.Close();
            }
            return lineID;
        }

        public static string GetUserSettings(string id)
        {
            using (var conn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["LINE_CONNECTION_STRING"]))
            {
                //id = "AFKW";
                var command = new SqlCommand("bn_Line_GetUserNotificationSettings", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("Line_ID", id));
                conn.Open();

                SqlDataReader reader = command.ExecuteReader();
                string ret = "Here's your setting configurations:\n";
                string state = "";
                while (reader.Read())
                {
                    ret += "\n";
                    state = Convert.ToInt16(reader["Value"]) == 1 ? "ON" : "OFF";
                    switch(reader["Settings"].ToString())
                    {
                        case "examNotification":
                            ret += "Exam Notification " + "[" + state + "]\n" + "  Send 'examNotification' to toggle";
                            break;
                        case "paymentNotification":
                            ret += "Payment Notification " + "[" + state + "]\n" + "  Send 'paymentNotification' to toggle";
                            break;
                        case "msgNotification":
                            ret += "Message Notification " + "[" + state + "]\n" + "  Send 'msgNotification' to toggle";
                            break;
                        case "classSchedNotification":
                            ret += "Class Schedule Notification " + "[" + state + "]\n" + "  Send 'classSchedNotification' to toggle";
                            break;
                    }
                }
                return ret;
            }
        }

        public static void ToggleNotificationSetting(string id, string message)
        {
            //id = "AFKW";
            using (var conn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["LINE_CONNECTION_STRING"]))
            {
                var command = new SqlCommand("bn_Line_SetUserNotificationSetting", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("Line_ID", id));
                command.Parameters.Add(new SqlParameter("message", message));
                conn.Open();

                command.ExecuteNonQuery();
                conn.Close();
            }
        }
    }
}