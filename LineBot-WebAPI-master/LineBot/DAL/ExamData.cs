﻿using LineBot.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace LineBot.DAL
{
    public class ExamData
    {
        public static string GetExamSchedule(string employeeID)
        {
            var acadCareer = LMSData.GetAcadCareer(employeeID);
            var strm = LMSData.GetPeriod(acadCareer, employeeID);
            using (var conn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["BM_EXAM_CONNECTION_STRING"]))
            {
                var command = new SqlCommand("Student_Exam_OwnSchedule", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("binusianid", employeeID));
                command.Parameters.Add(new SqlParameter("period", strm));
                conn.Open();

                var reader = command.ExecuteReader();
                string ret = "";
                string desc = "";
                while (reader.Read())
                {
                    if (ret != "")
                        ret += "\n";

                    if (desc != reader["DESCR"].ToString())
                    {
                        desc = reader["DESCR"].ToString();
                        ret += desc + "\n\n";
                    }

                    ret += reader["KDMTK"].ToString() + " - " + reader["COURSE_TITLE_LONG"].ToString() + " - " + reader["CLASS_SECTION"] +
                        " - " + reader["ExamDate"].ToString().Substring (0, reader["ExamDate"].ToString().Length - 9) + " " + reader["ExamStartTime"].ToString().Substring(0, 5) +
                        " - " + reader["LOCATION"].ToString() + " - " + reader["ROOM"].ToString() + " - " + reader["ChairNumber"].ToString();
                }

                if (ret == "")
                    ret = "No Exam Schedule";
                conn.Close();
                return ret;
            }
        }

        public static List<PushMessage> GetStudentExam()
        {
            var employeeID = "1701294991";
            var acadCareer = LMSData.GetAcadCareer(employeeID);
            var strm = LMSData.GetPeriod(acadCareer, employeeID);
            using (var conn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["LINE_CONNECTION_STRING"]))
            {
                strm = "1610";
                var command = new SqlCommand("bn_Line_Agent_GetStudentExam", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("period", strm));
                conn.Open();

                var reader = command.ExecuteReader();
                List<PushMessage> output = new List<PushMessage>();
                PushMessage data = new PushMessage();
                MessageObject msg = new MessageObject();
                msg.type = "text";
                string ret = "";
                string binusianID = "";
                while (reader.Read())
                {
                    if (binusianID != reader["BinusianID"].ToString())
                    {
                        if (binusianID != "")
                        {
                            data.messages.Add(msg);
                            output.Add(data);
                        }

                        data = new PushMessage();
                        ret = "";
                        binusianID = reader["BinusianID"].ToString();
                        data.to = reader["LineID"].ToString();
                    }

                    if (ret != "")
                        ret += "\n";

                    if (reader["DESCR"] == DBNull.Value)
                        continue;

                    //if (binusianID != reader["BinusianID"].ToString())
                    //{
                    //    binusianID = reader["BinusianID"].ToString();
                    //    if (ret != "")
                    //        ret += "\n\n";
                    //    ret += binusianID + "\n\n";
                    //}

                    ret += reader["DESCR"] + " - " + reader["KDMTK"].ToString() + " - " + reader["COURSE_TITLE_LONG"].ToString() + " - " + reader["CLASS_SECTION"] +
                        " - " + reader["ExamDate"].ToString() + " " + reader["ExamStartTime"].ToString().Substring(0, 5) +
                        " - " + reader["LOCATION"].ToString() + " - " + reader["ROOM"].ToString() + " - " + reader["ChairNumber"].ToString();

                    msg.text = ret;
                }
                data.messages.Add(msg);
                output.Add(data);

                conn.Close();
                return output;
            }
        }
    }
}