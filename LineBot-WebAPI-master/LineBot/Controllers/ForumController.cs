﻿using LineBot.Models;
using LineBot.Utility;
using LineBot.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LineBot.Controllers
{
    public class ForumController : BaseController
    {
        public ForumController(string token, string id, string msg) : base(token, id, msg)
        {
        }

        public ReplyModel Forum()
        {
            if (reply != null)
                return reply;

            reply = new ReplyModel();
            reply.replyToken = token;

            reply.messages = new List<ReplyMessage>()
                {
                    new ReplyMessage()
                    {
                        type = "text",
                        text = ForumData.GetForum(user.EmployeeID)
                    }
                };
            return reply;
        }
    }
}