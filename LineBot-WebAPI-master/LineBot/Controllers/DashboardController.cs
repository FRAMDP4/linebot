﻿using LineBot.Models;
using LineBot.Utility;
using LineBot.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace LineBot.Controllers
{
    public class DashboardController : BaseController
    {
        public DashboardController(string token, string id, string msg) : base(token, id, msg)
        {
        }

        public ReplyModel Course()
        {
            if (reply != null)
                return reply;

            reply = new ReplyModel();
            reply.replyToken = token;

            reply.messages = new List<ReplyMessage>()
                {
                    new ReplyMessage()
                    {
                        type = "text",
                        text = LMSData.GetCourse(user.EmployeeID)
                    }
                };
            return reply;
        }

        public ReplyModel Today()
        {
            if (reply != null)
                return reply;

            reply = new ReplyModel();
            reply.replyToken = token;

            reply.messages = new List<ReplyMessage>()
                {
                    new ReplyMessage()
                    {
                        type = "text",
                        text = LMSData.GetToday(user.EmployeeID)
                    }
                };
            return reply;
        }

        public ReplyModel Weekly()
        {
            if (reply != null)
                return reply;

            reply = new ReplyModel();
            reply.replyToken = token;

            reply.messages = new List<ReplyMessage>()
                {
                    new ReplyMessage()
                    {
                        type = "text",
                        text = LMSData.GetWeekly(user.EmployeeID, user.BinusianID)
                    }
                };
            return reply;
        }
    }
}