﻿using LineBot.Models;
using LineBot.Utility;
using LineBot.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LineBot.Controllers
{
    public class BaseController
    {
        private static readonly int STATUS_NOT_REGISTERED = 1;
        private static readonly int STATUS_OK = 2;
        private static readonly int STATUS_NO_PHONE_NUMBER = 3;
        private static readonly int STATUS_NO_NIM = 4;
        private static readonly int STATUS_OTP_NOT_VALIDATED = 5;
        protected LineUser user;
        protected ReplyModel reply;
        protected string token;
        protected string id;
        protected string msg;

        protected int checkRegistration(string id)
        {
            this.user = LineData.CheckUser(id);
            if (user == null)
                return STATUS_NOT_REGISTERED;
            else if (user.PhoneNumber == null || user.PhoneNumber == "")
                return STATUS_NO_PHONE_NUMBER;
            else if (user.BinusianID == null || user.BinusianID == "")
                return STATUS_NO_NIM;
            else if (user.OTP != -1)
                return STATUS_OTP_NOT_VALIDATED;
            return STATUS_OK;
        }

        public BaseController(string token, string id, string msg)
        {
            this.token = token;
            this.id = id;
            this.msg = msg;
            int status = checkRegistration(id);
            if (status == STATUS_NOT_REGISTERED)
            {
                reply = new ReplyModel();
                reply.replyToken = token;

                reply.messages = new List<ReplyMessage>()
                {
                    new ReplyMessage()
                    {
                        type = "text",
                        text = "Welcome!!! Please send your phone number"
                    }
                };
                LineData.Register(id, id.GetHashCode());
            }
            else if ((msg == "reset" && status == STATUS_OTP_NOT_VALIDATED) || (msg == "reset" && status == STATUS_NO_NIM))
            {
                LineData.resetUser(id);
                reply = new ReplyModel();
                reply.replyToken = token;

                reply.messages = new List<ReplyMessage>()
                {
                    new ReplyMessage()
                    {
                        type = "text",
                        text = "Your Registration data has been removed. Send start to trigger the bot again"
                    }
                };
            }
            else if (status == STATUS_NO_PHONE_NUMBER)
            {
                msg = msg.Replace("+", "");
                msg = msg.Replace("-", "");
                msg = msg.Replace(" ", "");
                msg = msg.Replace("(", "");
                msg = msg.Replace(")", "");
                
                if (msg.Length < 9 || msg.Length > 13)
                {
                    reply = new ReplyModel();
                    reply.replyToken = token;

                    reply.messages = new List<ReplyMessage>()
                    {
                        new ReplyMessage()
                        {
                            type = "text",
                            text = "Invalid! Please send your phone number"
                        }
                    };
                }
                else
                {
                    LineData.UpdatePhoneNumber(id, msg);
                    reply = new ReplyModel();
                    reply.replyToken = token;

                    reply.messages = new List<ReplyMessage>()
                    {
                        new ReplyMessage()
                        {
                            type = "text",
                            text = "Please send your NIM"
                        }
                    };
                }

            }
            else if (status == STATUS_NO_NIM)
            {
                int n;
                if (msg.Length == 10 && int.TryParse(msg, out n))
                {
                    LineData.UpdateNIM(id, msg);
                    reply = new ReplyModel();
                    reply.replyToken = token;

                    reply.messages = new List<ReplyMessage>()
                    {
                        new ReplyMessage()
                        {
                            type = "text",
                            text = "Please send OTP Code you get from Binusmaya (Menu -> Registration -> Line Registration)"
                        }
                    };
                }
                else
                {
                    reply = new ReplyModel();
                    reply.replyToken = token;

                    reply.messages = new List<ReplyMessage>()
                    {
                        new ReplyMessage()
                        {
                            type = "text",
                            text = "Please send your NIM"
                        }
                    };
                }
            }
            else if (status == STATUS_OTP_NOT_VALIDATED)
            {
                if (msg == user.OTP.ToString())
                {
                    //validateOTP
                    LineData.validateOTP(id, int.Parse(msg));
                    reply = new ReplyModel();
                    reply.replyToken = token;

                    reply.messages = new List<ReplyMessage>()
                    {
                        new ReplyMessage()
                        {
                            type = "text",
                            text = "Your NIM Validated! Send 'help' to get started"
                        }
                    };
                }
                else
                {
                    reply = new ReplyModel();
                    reply.replyToken = token;

                    reply.messages = new List<ReplyMessage>()
                    {
                        new ReplyMessage()
                        {
                            type = "text",
                            text = "Please send OTP Code you get from Binusmaya (Menu -> Registration -> Line Registration)"
                        }
                    };
                }
            }
            else if (msg == "start")
            {
                LineData.insertLog(id, msg);
                reply = new ReplyModel();
                reply.replyToken = token;

                reply.messages = new List<ReplyMessage>()
                    {
                        new ReplyMessage()
                        {
                            type = "text",
                            text = "Welcome back!"
                        }
                    };
            }
            else
            {
                LineData.insertLog(id, msg);
            }
        }

        public ReplyModel HandleMessage()
        {
            if (reply != null)
                return reply;

            reply = new ReplyModel();
            reply.replyToken = token;

            reply.messages = new List<ReplyMessage>()
                {
                    new ReplyMessage()
                    {
                        type = "text",
                        text = "Wrong command sent! Send 'help' to see what this bot can do."
                    }
                };
            return reply;
        }
    }
}