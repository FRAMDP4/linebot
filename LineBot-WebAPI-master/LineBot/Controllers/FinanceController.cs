﻿using LineBot.Models;
using LineBot.Utility;
using LineBot.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LineBot.Controllers
{
    public class FinanceController : BaseController
    {
        public FinanceController(string token, string id, string msg) : base(token, id, msg)
        {
        }

        public ReplyModel Finance()
        {
            if (reply != null)
                return reply;

            reply = new ReplyModel();
            reply.replyToken = token;

            reply.messages = new List<ReplyMessage>()
                {
                    new ReplyMessage()
                    {
                        type = "text",
                        text = FinanceData.GetFinance(user.BinusianID, user.EmployeeID)
                    }
                };
            return reply;
        }
    }
}