﻿using LineBot.DAL;
using LineBot.Models;
using LineBot.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace LineBot.Controllers
{
    public class NotifierController
    { 
        public async Task<HttpResponseMessage> notifyExam()
        {
            //TODO query dulu semua exam disini
            NotificationSender.sendNotification(ExamData.GetStudentExam());
            return new HttpResponseMessage(System.Net.HttpStatusCode.OK);
            //output: List<msg> -> 2
        }

        public async Task<HttpResponseMessage> notifyMessage(string employeeID, int specificRoleID)
        {
            var msg = LMSData.GetNewestMessage(employeeID, specificRoleID);
            if(msg.to != null)
                await NotificationSender.sendNotification(msg);
            return new HttpResponseMessage(System.Net.HttpStatusCode.OK);
            
        }

        public async Task<HttpResponseMessage> notifyTodaySchedule()
        {
            NotificationSender.sendNotification(LMSData.GetClassScheduleNextAgenda());
            return new HttpResponseMessage(System.Net.HttpStatusCode.OK);
        }
    }
}