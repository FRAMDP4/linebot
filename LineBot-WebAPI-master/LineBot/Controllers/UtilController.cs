﻿using LineBot.Models;
using LineBot.Utility;
using LineBot.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LineBot.Controllers
{
    public class UtilController : BaseController
    {
        public UtilController(string token, string id, string msg) : base(token, id, msg)
        {
        }

        public ReplyModel Help()
        {
            if (reply != null)
                return reply;
            reply = new ReplyModel();
            reply.replyToken = token;

            reply.messages = new List<ReplyMessage>()
                {
                    new ReplyMessage()
                    {
                        type = "text",
                        text = "Bee Bot\n\nBinusian Bot to help Binusian access information that useful and relevant to them such as schedule, news, message, and many more! Don’t forget to send us your feedback.\n\nhelp : See what bot can do\nstart : Start using Binusian Bot\nreset : Reset your line registration data\ncourse : Get your course list for active semester\ntoday : Get your schedule for today\nweekly : Get your weekly schedule\ngslc : Get next 3 GSLC Schedules\nforum : Get latest forum threads\nmessage : Get latest message\nnews : Get latest news\nfinance : Get your financial report and status\nexam : Get your exam schedule\nscore : Get your exam scores\nsettings : Change your notification settings"
                    }
                };
            return reply;
        }

        public ReplyModel Ping()
        {
            if (reply != null)
                return reply;
            reply = new ReplyModel();
            reply.replyToken = token;

            reply.messages = new List<ReplyMessage>()
                {
                    new ReplyMessage()
                    {
                        type = "text",
                        text = "Hey There!"
                    }
                };
            return reply;
        }

        public ReplyModel Reset()
        {
            if (reply != null)
                return reply;
            LineData.resetUser(id);
            reply = new ReplyModel();
            reply.replyToken = token;

            reply.messages = new List<ReplyMessage>()
                {
                    new ReplyMessage()
                    {
                        type = "text",
                        text = "Your Registration data has been removed. Send start to trigger the bot again"
                    }
                };
            return reply;
        }
    }
}