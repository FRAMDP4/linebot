﻿using LineBot.Models;
using LineBot.Utility;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace LineBot.Controllers
{
    public class BroadcastController : ApiController
    {
        public async Task<HttpResponseMessage> Post()
        {
            try
            {
                var body = await Request.Content.ReadAsStringAsync();
                var value = JsonConvert.DeserializeObject<BroadcastRequest>(body);
                if (value.apiKey == Util.API_KEY)
                {
                    MessageBroadcaster.SendBroadcast(DBHelper.GetAllLineUserID(), value.data.message);
                    return new HttpResponseMessage(System.Net.HttpStatusCode.OK);
                }
                return new HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
            }
            catch(Exception ex)
            {
                Console.Write(ex.StackTrace);
                return new HttpResponseMessage(System.Net.HttpStatusCode.InternalServerError);
            }
        }
    }
}