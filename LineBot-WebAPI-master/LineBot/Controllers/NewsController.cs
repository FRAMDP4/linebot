﻿using LineBot.Models;
using LineBot.Utility;
using LineBot.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LineBot.Controllers
{
    public class NewsController : BaseController
    {
        public NewsController(string token, string id, string msg) : base(token, id, msg)
        {
        }

        public ReplyModel News()
        {
            if (reply != null)
                return reply;

            reply = new ReplyModel();
            reply.replyToken = token;

            reply.messages = new List<ReplyMessage>()
                {
                    new ReplyMessage()
                    {
                        type = "text",
                        text = LMSData.GetNews(user.BinusianID, user.RoleID)
                    }
                };
            return reply;
        }
    }
}