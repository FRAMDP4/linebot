﻿using LineBot.Models;
using LineBot.Utility;
using LineBot.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LineBot.Controllers
{
    public class MessageController : BaseController
    {
        public MessageController(string token, string id, string msg) : base(token, id, msg)
        {
        }

        public ReplyModel Message()
        {
            if (reply != null)
                return reply;

            reply = new ReplyModel();
            reply.replyToken = token;

            reply.messages = new List<ReplyMessage>()
                {
                    new ReplyMessage()
                    {
                        type = "text",
                        text = LMSData.GetMessage(user.EmployeeID, user.SpecificRoleID)
                    }
                };
            return reply;
        }
    }
}