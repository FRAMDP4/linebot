﻿using LineBot.Models;
using LineBot.Utility;
using LineBot.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LineBot.Controllers
{
    public class SettingsController : BaseController
    {
        public SettingsController(string token, string id, string msg) : base(token, id, msg)
        {
        }

        public ReplyModel Settings()
        {
            if (reply != null)
                return reply;

            reply = new ReplyModel();
            reply.replyToken = token;

            reply.messages = new List<ReplyMessage>()
                {
                    new ReplyMessage()
                    {
                        type = "text",
                        text = LineData.GetUserSettings(user.LineID)
                    }
                };
            return reply;
        }

        public ReplyModel NotificationSettingSaved()
        {
            if (reply != null)
                return reply;

            reply = new ReplyModel();
            reply.replyToken = token;

            LineData.ToggleNotificationSetting(user.LineID, msg);

            reply.messages = new List<ReplyMessage>()
                {
                    new ReplyMessage()
                    {
                        type = "text",
                        text = "Setting saved"
                    }
                };
            return reply;
        }
    }
}